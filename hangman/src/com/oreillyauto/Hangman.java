package com.oreillyauto;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Your task is to complete the game "Hangman."
 * - You have words.txt and word-v2.txt to use as a wordlist.
 * - You can build your own word file if you want.
 * - Reference Helper.java as some of the helper (stubbed) functions 
 *   will assist (speed up) your development.
 *   
 * - Please refer to https://www.wikihow.com/Play-Hangman for 
 * - game rules.
 * 
 * @author Justin Case
 *   
 */
public class Hangman {    
    private static final String HANGMAN_LEGS = "legs";
    private static final String HANGMAN_LOWERTORSO = "lowertorso";
    private static final String HANGMAN_UPPERTORSO = "uppertorso";
    private static final String HANGMAN_HEAD = "head";
    public boolean gameOver = false;
    public static BufferedReader consoleReader; // Using a BufferedReader instead of Scanner
    public List<String> wordList = new ArrayList<String>();
    Random ran = new Random();
    public int wordListIndex = ran.nextInt(28558); 
    public int fails = 0;                       // Number of fails (7 fails total before end of game)
    public String message = "";                 // Message to the user for a given turn
    public String currentWord = "";             // Current hangman word
    public String wordUnderscores = "";         // Underscore and letters string
    public String option = "";                  // Current selected user option from the console
    public List<Character> correctCharList;     // Correct character guesses (not initialized)
    public List<Character> incorrectCharList;   // Incorrect character guesses (not initialized)
    public Hangman hangman;
    
    public Hangman() {
    	// Declare Variables
    	// Get access to the Hangman() we created via "this" because
    	// we pass the hangman object to the Helper class in some cases.
        hangman = this; 
        
        // BufferedReader is synchronous while Scanner is not. 
        // BufferedReader has significantly larger buffer memory than Scanner.
        consoleReader = new BufferedReader(new InputStreamReader(System.in)); 
        
        // INITIALIZE YOUR 2 LISTS HERE!!!!!!!!!!!!!!!!!
        // (correctCharList and incorrectCharList)
        correctCharList = new ArrayList<Character>();
        incorrectCharList = new ArrayList<Character>();
        
        // Load the words from the word file into memory
        Helper.loadWordsIntoMemory(wordList);

        // Loop - draw the board, get user selection, and provide feedback
        while (true) {
            // Draw the board (menu, hangman area, underscores)
            drawBoard();
            
            // Get the user selection. It can be an integer or 
            // a String, so an Object is returned.
            Object userSelection = Helper.getUserOption(consoleReader);
            
            // Process the user selection
            if (userSelection instanceof Integer) {
            	// If the user entered a number, then we need to
            	// process this selection. It is most likely a 
            	// menu option.
                processNumericSelection((Integer)userSelection);
            } else {
            	// If the user did NOT enter a number, they
            	// must have entered a letter OR guessed
            	// the word.
                processAlphaSelection((String)userSelection);
            }
        }
    }

    /**
     * In this method, we need to figure out what the user wants
     * to do with the menu option that they selected. Notice that 
     * option is a global variable. We can assess what to do for 
     * a given input based on the option selected.
     * @param userSelection
     */
    private void processNumericSelection(Integer userSelection) {
        
        switch (userSelection) {
            case 1:
                // Submit a letter
                option = "1";
                break;
            case 2:
                // Guess the word
                option = "2";
                break;
            case 3:
                // Give up
                option = "3";
                break;
            case 4:
                // Quit
                option = "4";
                System.out.println("Goodbye!");
                System.exit(0);
                break;
            default:
                message = ""; // set error message (for invalid selection)
        }
        
    }

    /**
     * Process Alpha Selection
     * At this point the user must have selected a letter or 
     * guessed the word. Don't forget that "option" is global. 
     * You are using a switch to determine their previous option 
     * they selected so you can determine what to do with the 
     * character or string that they entered.
     * @param userSelection
     */
    private void processAlphaSelection(String userSelection) {
        switch (option) {
            case "1": // User selected option 1 (guess a letter)
                char c = ' '; // Get the character!
                Helper.setMessage(option, gameOver, currentWord, hangman);
                processLetterChoice(c);
                break;
            case "2": // User selected option 2 (guess the word)
                processWordGuess(userSelection);
                Helper.setMessage(option, gameOver, currentWord, hangman);
                break;
            default :
                System.out.println("Invalid Selection!");
                break;
        }
    }

    /**
     * Process Word Guess
     * You need to cross reference the user's console entry here
     * with the word they need to guess.
     * @param userSelection
     */
    private void processWordGuess(String userSelection) {
        // Check if word is correct
    	System.out.println("processing word");
    	
    	// Increment fails if applicable
    	
    	
    }

    /**
     * 
     * @param c
     */
    private void processLetterChoice(char c) {  	
        // See if letter is correct
    	System.out.println("processing letter");
    	
        // update lists
    	
    	
    	// Increment fails if applicable
    	
    }
    
    /**
     * Set Message
     * Our message variable is global. This method is global. We can
     * call this method from the Helper method when applicable.
     * @param message
     */
    public void setMessage(String message) {
        this.message = message;
    }
    
    /**
     * Set Word Underscores
     * Our wordUnderscores variable is global. This method is global. We can
     * call this method from the Helper method when applicable.
     * @param wordUnderscores
     */
    public void setWordUnderscores(String wordUnderscores) {
        this.wordUnderscores = wordUnderscores;
    }

    /**
     * Set Current Word
     * Our currentWord variable is global. This method is global. We can
     * call this method from the Helper method when applicable.
     * @param currentWord
     */
    public void setCurrentWord(String currentWord) {
        this.currentWord = currentWord;
    }
    
    /**
     * Draw Board
     * This method draws the board in the console. The Helper method
     * calls a system command to clear the console. This does NOT work 
     * in Eclipse but works well in the Windows console. Check out the
     * document "Creating a runnable jar" in the Google Drive. If you run
     * your Hangman application from a windows console, your console will
     * clear successfully when the board is redrawn.
     */
    private void drawBoard() {
        String parts = "";
        // Clear the console each time we want to re-draw the board
        Helper.clearTheConsole();
        
        // Set messages in context - You may want to give instructions
        // to the user at this point. 
        Helper.setMessage(option, gameOver, currentWord, hangman);
        
        // Build the underscore string
        Helper.populateWordUnderscores(wordList, wordListIndex, hangman, correctCharList);
        
        // Do not remove. The clearTheConsole call adds an 
        // odd character in Eclipse. We will build a new line
        System.out.println("");
        
        // By default, the entire hangman is drawn to the console.
        // You will need to remove the actual drawing of the man.
        // Notice the numeric spaces needed to draw the pole/man.
        // You may see 2 backslashes to draw an arm or a leg. This is
        // necessary as we need to escape the backslash with a backslash
        // in order to draw it properly in the console.
        System.out.println("******* Hangman 1.0 *******    |----");
        // (31 spaces to the pole and 35 spaces to the head)
        parts = (fails > 0) ? "   O" : "";
        System.out.print  ("Options:                       |" + parts); // O   "); // 31-35 
        drawHangman(HANGMAN_HEAD);
        // (31 spaces to the pole and 34 spaces to the left arm)
        parts = "";
        if(fails >= 4 ) {
            parts = "  /|\\";
        } else if(fails >= 3) {
            parts = "  /|";
        } else if (fails >= 2) {
            parts = "  /";
        }
        System.out.print  ("1. Submit a letter             |" + parts); // /|\\ "); // 31-34 
        drawHangman(HANGMAN_UPPERTORSO);
        parts = (fails > 4) ? "   |" : "";
        System.out.print  ("2. Guess the word              |" + parts); // |   "); // 31-35
        drawHangman(HANGMAN_LOWERTORSO);
        if(fails >= 6) {
            parts = "  / \\";
        } else if(fails >= 5) {
            parts = "  /";
        }
        System.out.print  ("3. Give up on this word        |" + parts); // / \\ "); // 31-34
        drawHangman(HANGMAN_LEGS);
        System.out.println("4. Quit                      __|__     " + wordUnderscores);
        System.out.println("");
        System.out.println(message);
        
        // Clear the message after we show it
        message = "";
        
        // Need to build a new board to handle 2 new options
        // once the user wins or loses
        // 1. Play Again
        // 2. Quit
    }

    /**
     * Draw Hangman Body Parts
     * The number of fails is global so we can use this information
     * in order to know how to draw the hangman.
     * @param bodyPart
     */
    private void drawHangman(String bodyPart) {
        switch(bodyPart) {
            case HANGMAN_HEAD:
                System.out.println("");
                break;
            case HANGMAN_UPPERTORSO:
                switch(fails) {
                     
                }
                System.out.println("");
                break;
            case HANGMAN_LOWERTORSO:
                System.out.println("");
                break;
            case HANGMAN_LEGS:
                switch(fails) {
                }
                System.out.println("");
                break;
        }
    }

    /**
     * Main - Entry point into the application
     * @param args
     */
    public static void main(String[] args) {
        new Hangman();
    }

}
