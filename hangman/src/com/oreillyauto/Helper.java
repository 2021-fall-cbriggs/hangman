package com.oreillyauto;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Stream;

/**
 * This class is designed to help you add logic to your application. Notice that the methods are static. You can access the methods by
 * calling the class and static method without instantiating the Helper class: Helper.loadWordsIntoMemory(List<String> wordList); Whenever
 * you see "Hangman hangman" as a parameter, this is an indication that you can make method calls back to the Hangman class. This
 * communication is often necessary. hangman.setMessage("Game Over!");
 * 
 * @author jbrannon5
 */
public class Helper {

    /**
     * Load Words in Memory This method loads words into memory
     * 
     * @param wordList
     */
    public static void loadWordsIntoMemory(List<String> wordList) {

        /**
         * Load words into memory from execution path (root of the project)
         */
        try (Stream<String> stream = Files.lines(Paths.get("words-v2.txt"))) {
            stream.forEach(word -> wordList.add(word.trim()));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Populate Word Underscores
     * We want to present the underscores to the user so they can see
     * how many letters are in the current word.
     * @param wordList
     * @param wordListIndex
     * @param hangman
     * @param correctCharList
     */
    public static void populateWordUnderscores(List<String> wordList, 
            Integer wordListIndex, Hangman hangman, List<Character> correctCharList) {
        
        if (wordList.size() == 0) {
            hangman.setMessage("Error: Word List Is Empty!!!!");
        } else {
            String word = wordList.get(wordListIndex);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i<word.length() ; i++) {
                if (correctCharList.contains(word.charAt(i))){
                    sb.append(word.charAt(i)+" ");               
                } else {
                    sb.append("_ ");
                }
            }
            // Build the underscores and characters
            // For example: the word "intern" should look like "_ _ _ _ _ _"
            
            
            //... 
            
            hangman.setWordUnderscores(sb.toString());
        }
    }

    /**
     * Set Message
     * 
     * @param option
     * @param gameOver
     * @param currentWord
     * @param hangman
     */
    public static void setMessage(String option, boolean gameOver, String currentWord, Hangman hangman) {
        // Set a user message based on the current user selected option
        // e.g. if user selected option 1 then show the message "Please select a letter" 
        //      hangman.setMessage("Please select a letter");

        if (option.length() == 0) {
            hangman.setMessage("Please selection an option.");
        } else {
            // Manage Numeric Options
            if (isNumeric(option)) {
                switch(option) {
                    case "1": 
                        hangman.setMessage("Please input a letter.");
                        break;
                    case "2":
                        hangman.setMessage("Please input a word.");
                    
                      
                }
            } else {
                // Manage Non-Numeric Options

            }
        }
    }

    /**
     * Is Numeric Checks if the string that is passed in is an Integer or not
     * 
     * @param option
     * @return
     */
    private static boolean isNumeric(String str) {
        try {
            Integer.parseInt(str);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Clear the Console This method works great in the Windows console. It does NOT clear the console in Eclipse unfortunately. It,
     * instead, will print a weird character. =>
     */
    public static void clearTheConsole() {
        try {
            Thread.sleep(500);
            new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Get User Option This method uses the BufferedReader class to read the console.
     * 
     * @param consoleReader
     * @return
     */
    public static Object getUserOption(BufferedReader consoleReader) {
        try {
            String response = consoleReader.readLine();

            if (response != null) {
                return (Helper.isNumeric(response)) ? Integer.parseInt(response) : response;
            } else {
                return null;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
